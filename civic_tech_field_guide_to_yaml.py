#! /usr/bin/env python3

# civic-tech-field-guide-to-yaml -- Convert Civic Tech Field Guide spreadsheet to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2016 Etalab
# https://git.framasoft.org/codegouv/civic-tech-field-guide-to-yaml
#
# civic-tech-field-guide-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# civic-tech-field-guide-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import codecs
import collections
import csv
import logging
import os
import sys
import urllib.request

from slugify import slugify
import yaml


# YAML configuration


class folded_unicode(str):
    pass


class literal_unicode(str):
    pass


def dict_constructor(loader, node):
    return collections.OrderedDict(loader.construct_pairs(node))


def dict_representer(dumper, data):
    return dumper.represent_dict(sorted(data.items()))


yaml.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, dict_constructor)

yaml.add_representer(folded_unicode, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='>'))
yaml.add_representer(literal_unicode, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='|'))
yaml.add_representer(dict, dict_representer)
yaml.add_representer(collections.OrderedDict, dict_representer)
yaml.add_representer(str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str', data))


#


app_name = os.path.splitext(os.path.basename(__file__))[0]
args = None
log = logging.getLogger(app_name)
spreadsheet_name = 'Common Tool Types and Function'
spreadsheet_url = 'https://docs.google.com/spreadsheets/d/1FzmvVAKOOFdixCs7oz88cz9g1fFPHDlg0AHgHCwhf4A/export?' \
    + 'format=csv&id=1FzmvVAKOOFdixCs7oz88cz9g1fFPHDlg0AHgHCwhf4A&gid=0'


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('yaml_dir', help = 'path of target directory for generated YAML files')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    global args
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    if not os.path.exists(args.yaml_dir):
        os.makedirs(args.yaml_dir)

    if not os.path.exists(args.yaml_dir):
        os.makedirs(args.yaml_dir)

    response = urllib.request.urlopen(spreadsheet_url)
    reader = csv.reader(codecs.iterdecode(response, 'utf-8'))
    comments_row = next(reader)
    categories_row = next(reader)
    categories_row = [
        category_clean
        for category_clean in (
            category.strip()
            for category in categories_row
            )
        ]
    descriptions_row = next(reader)
    for row in reader:
        for category, name in zip(categories_row, row):
            if not category:
                continue
            name = name.strip()
            if not name or name == category:
                continue
            entry = dict(
                category = category,
                name = name,
                )
            slug = slugify(name)
            with open(os.path.join(args.yaml_dir, '{}.yaml'.format(slug)), 'w') as yaml_file:
                yaml.dump(entry, yaml_file, allow_unicode = True, default_flow_style = False, indent = 2, width = 120)

    return 0


if __name__ == "__main__":
    sys.exit(main())

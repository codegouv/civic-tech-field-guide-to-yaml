# civic-tech-field-guide-to-yaml

Convert [Civic Tech Field Guide spreadsheet](http://bit.ly/organizecivictech) to a Git repository of YAML files.

## Usage

```bash
./civic_tech_field_guide_to_yaml.py ../civic-tech-field-guide-yaml/
```
